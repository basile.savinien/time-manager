#!/bin/bash
# DOCKER STOP
docker-compose stop
mkdir -p db
# DOCKER BUILD

docker-compose build  --no-cache

sed -i 's/APP_VERSION\\=.*/APP_VERSION=${build_version}/' .env

# DOCKER DB
docker-compose up -d db

# DOCKER RUN
docker-compose run phoenix mix ecto.drop
docker-compose run phoenix mix ecto.create
docker-compose run phoenix mix ecto.migrate
docker-compose run phoenix mix run priv/repo/seeds.exs
docker-compose run phoenix mix phx.swagger.generate

# DOCKER STOP
docker-compose stop
docker-compose up -d