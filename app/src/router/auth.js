import decode from 'jwt-decode';
import axios from 'axios';
import config from '@/Utils/config.json';

const AUTH_TOKEN_KEY = 'accesToken';

export function isLoggedIn() {
    let authToken = localStorage.getItem(AUTH_TOKEN_KEY)
    return !!authToken
}

export function getUserInfo() {
    if (isLoggedIn()) {
        return decode(getAuthToken())
    }
}

export function getAuthToken() {
    return localStorage.getItem(AUTH_TOKEN_KEY)
}

export function getUserId() {
    const tmp = getUserInfo();
    if (!tmp)
        return undefined;
    const userId = tmp.aud.split(':')[1];
    if (!userId)
        return undefined
    return userId;
}

export async function getEmailAddress() {
    return axios.get(`${config.config.res_endpoint}api/users/${getUserId()}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("accesToken"),
        },
    }).then((response) => {
        return response.data.data.email;
    }).catch(() => {
        return undefined;
    });
}

export async function getUserName() {
    return await axios.get(`${config.config.res_endpoint}api/users/${getUserId()}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("accesToken"),
        },
    }).then((response) => {
        var data = response.data.data.username;
        return data;
    }).catch(() => {
        return undefined;
    });
}

export function getUserRole() {
    return axios.get(`${config.config.res_endpoint}api/users/${getUserId()}`, {
        headers: {
            Authorization: "Bearer " + localStorage.getItem("accesToken"),
        },
    }).then((response) => {
        var data = response.data.role;
        return data;
    })
}