import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Register from '../views/Register.vue'
import About from '../views/About.vue'
import Log from '../views/Log.vue'
import Manager from '../views/Manager'
import { isLoggedIn } from './auth.js'
import { getUserRole } from './auth.js'
import Managertime from '../components/Manager/Managertime'
import Profile from "../components/Logs/Profile"

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'about',
      component: About,
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'User',
      component: User
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/manageuser',
      name: 'Manager',
      component: Manager
    },
    {
      path: '/managetime',
      name: 'Managertime',
      component: Managertime
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'Log',
      component: Log,
      meta: {
        allowAnonymous: true
      }
    },
  ]
})

router.beforeEach( async (to, from, next) => {
  if ((to.name == 'Log' || to.name == 'about') && isLoggedIn()) {
    next({ path: '/Home' })
  }
  else if (!to.meta.allowAnonymous && !isLoggedIn()) {
    next({
      path: '/login',
    })
  }
  else if (isLoggedIn()) {
    const role = await getUserRole()
    if ((to.name == 'Manager' || to.name == 'Managertime') && role == 'user') {
      next({ path: '/Home' })
    }
    else {
      next()
    }
  }
  else {
    next()
  }
})

export default router