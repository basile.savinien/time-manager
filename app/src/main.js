import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.silent = true;

new Vue({
  router,
  el: '#app',

  render: h => h(App)
})
