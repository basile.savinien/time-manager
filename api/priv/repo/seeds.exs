# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Timemanager.Repo.insert!(%Timemanager.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

import Ecto
import Ecto.Query, warn: false
alias Timemanager.Repo
alias Timemanager.Gotham.User

Repo.insert! %User{
  username: "admin",
  email: "admin@gothamcity.com",
  role: "admin",
  password_hash: Comeonin.Bcrypt.hashpwsalt("Admin1234!")
}
