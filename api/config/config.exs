# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :timemanager,
  ecto_repos: [Timemanager.Repo]

# Configures the endpoint
config :timemanager, TimemanagerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VZcgXsdoAYGbD5pVQ1ol18BbEfpaPhAOvIJrS7de3XpPbfzT6vOeSqpDvc1Ynwmd",
  render_errors: [view: TimemanagerWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Timemanager.PubSub,
  live_view: [signing_salt: "LC8cGgWk"]

config :timemanager, :phoenix_swagger,
       swagger_files: %{
         "priv/static/swagger.json" => [
           router: TimemanagerWeb.Router,     # phoenix routes will be converted to swagger paths
         ]
       }

config :guardian, Guardian,
       issuer: "Timemanager",
       ttl: { 30, :days },
       allowed_drift: 2000,
       secret_key: "mUw5HAvOPGi1QZsQ7qeZLugawdFSR4JUTpA0kmstxavbPCWWNbgRYTUK77uAveVw", # Insert previously generated secret key!
       serializer: Timemanager.GuardianSerializer

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
config :phoenix_swagger, json_library: Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
