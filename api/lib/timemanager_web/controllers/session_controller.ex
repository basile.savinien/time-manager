defmodule TimemanagerWeb.SessionController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Gotham.User

  action_fallback(TimemanagerWeb.FallbackController)

  def swagger_definitions do
    %{
      Session:
        swagger_schema do
          title("Session")
          description("Session schema")

          properties do
            email(:string, "The email", required: true)
            password(:string, "The password", required: true)
          end

          example(
            %{
              session: %{
                email: "test@test.com",
                password: "Corentin60!"
              }
            }
          )
        end
    }
  end

  swagger_path :sign_in do
    tag("Sessions")
    post("/sign_in")

    parameters do
      session(:body, Schema.ref(:Session), "Sign in user", required: true)
    end

    response(200, "Success")
  end

  def sign_in(conn, %{"session" => %{"email" => email, "password" => password}}) do
    case User.find_and_confirm_password(email, password) do
      {:ok, user} ->
        {:ok, jwt, _full_claims} = Guardian.encode_and_sign(user, :api)

        conn
        |> render("sign_in.json", user: user, jwt: jwt)
      {:error, _reason} ->
        conn
        |> put_status(401)
        |> render("error.json", message: "Could not login")
    end
  end
  def sign_out(conn, _) do
    conn
    |> Guardian.Plug.sign_out() #This module's full name is Auth.UserManager.Guardian.Plug,
    |> render("sign_out.json", message: "Log out ok")
  end
end
