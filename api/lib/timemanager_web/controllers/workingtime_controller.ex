defmodule TimemanagerWeb.WorkingtimeController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger
  alias Timemanager.Gotham
  alias Timemanager.Gotham.Workingtime
  # alias Timemanager.Gotham.User
  # alias Timemanager.Repo

  action_fallback(TimemanagerWeb.FallbackController)

  def swagger_definitions do
    %{
      Workingtime:
        swagger_schema do
          title("Workingtime")
          description("Workingtime schema")

          properties do
            start_date(:datetime, "Clock's time", required: true)
            end_date(:datetime, "Clock's time", required: true)
            user(:User, "The user", required: true)
          end

          example(%{
            workingtime: %{
              start_date: "2019-03-01 03:00:02.123+02:00",
              end_date: "2019-03-06 03:00:02.123+02:00",
              user: "1"
            }
          })
        end
    }
  end

  swagger_path :create do
    tag("Workingtimes")
    post("/workingtimes/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
      workingtime(:body, Schema.ref(:Workingtime), "Create Clock", required: true)
    end

    response(200, "Success")
  end

  swagger_path :index do
    tag("Workingtimes")
    get("/workingtimes/{userId}/{workingtimeId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
      workingtimeId(:path, :string, "The workingtime id", required: true)
    end

    response(200, "Success")
  end

  swagger_path :delete do
    tag("Workingtimes")
    PhoenixSwagger.Path.delete("/workingtimes/{workingtimeId}")

    parameters do
      workingtimeId(:path, :string, "The workingtime id", required: true)
    end

    response(200, "Success")
  end

  swagger_path :update do
    tag("Workingtimes")
    put("/workingtimes/{workingtimeId}")

    parameters do
      workingtimeId(:path, :string, "The workingtime id", required: true)
      workingtime(:body, Schema.ref(:Workingtime), "Update workingtime", required: true)
    end

    response(200, "Success")
  end

  def index(conn, params) do
    user_id = Map.get(params, "userID")
    workingtime_id = Map.get(params, "workingtimeID")
    workingtimes = Gotham.get_time_by_user_id(user_id, workingtime_id)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def create(conn, %{"userID" => id, "workingtime" => workingtime_params}) do
    user = Gotham.get_user!(id)

    with {:ok, %Workingtime{} = workingtime} <-
           Gotham.create_workingtime(workingtime_params, user) do
      conn
      |> put_status(:created)
      |> render("show.json", workingtime: workingtime)
    end
  end

  def show(conn, %{"id" => id}) do
    workingtime = Gotham.get_workingtime!(id)
    render(conn, "show.json", workingtime: workingtime)
  end

  def update(conn, %{"id" => id, "workingtime" => workingtime_params}) do
    workingtime = Gotham.get_workingtime!(id)

    with {:ok, %Workingtime{} = workingtime} <-
           Gotham.update_workingtime(workingtime, workingtime_params) do
      render(conn, "show.json", workingtime: workingtime)
    end
  end

  def delete(conn, %{"id" => id}) do
    workingtime = Gotham.get_workingtime!(id)

    with {:ok, %Workingtime{}} <- Gotham.delete_workingtime(workingtime) do
      render(conn, "show.json", workingtime: workingtime)
    end
  end


  def all(conn, params) do
    user_id = Map.get(params, "userID")
    workingtimes = Gotham.get_workingtimes(user_id)
    render(conn, "index.json", workingtimes: workingtimes)
  end
end
