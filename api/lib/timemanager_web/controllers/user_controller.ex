defmodule TimemanagerWeb.UserController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Gotham
  alias Timemanager.Gotham.User
  alias Timemanager.Repo

  action_fallback(TimemanagerWeb.FallbackController)

  def swagger_definitions do
    %{
      User:
        swagger_schema do
          title("User")
          description("User schema")

          properties do
            username(:string, "The username of the user", required: true)
            email(:string, "The email", required: true)
            password(:string, "The password", required: true)
          end

          example(
            %{
              user: %{
                username: "Carbon",
                email: "test@test.com",
                password: "Corentin60!"
              }
            }
          )
        end
    }
  end

  swagger_path :show do
    tag("Users")
    get("/users/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
    end

    response(200, "Success")
  end

  swagger_path :delete do
    tag("Users")
    PhoenixSwagger.Path.delete("/users/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
    end

    response(200, "Success")
  end

  swagger_path :create do
    tag("Users")
    post("/users")

    parameters do
      user(:body, Schema.ref(:User), "Create User", required: true)
    end

    response(200, "Success")
  end

  swagger_path :update do
    tag("Users")
    put("/users/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
      user(:body, Schema.ref(:User), "Update User", required: true)
    end

    response(200, "Success")
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> put_status(:created)
        |> render("user.json", user: user)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> put_view(TimemanagerWeb.ChangesetView)
        |> render("error.json", changeset: changeset)
    end
  end

  def index(conn, _params) do
    users = Gotham.list_users()
    render(conn, "index.json", users: users)
  end

  def show(conn, %{"userID" => id}) do
    user = Gotham.get_user!(id)
    render(conn, "user.json", user: user)
  end

  def update(conn, %{"userID" => id, "user" => user_params}) do
    user = Gotham.get_user!(id)

    with {:ok, %User{} = user} <- Gotham.update_user(user, user_params) do
      render(conn, "user.json", user: user)
    end
  end

  def delete(conn, %{"userID" => id}) do
    user = Gotham.get_user!(id)

    with {:ok, %User{}} <- Gotham.delete_user(user) do
      render(conn, "user.json", user: user)
    end
  end

  def search(conn, %{}) do
    params = conn.query_params

    user = Gotham.get_user_by_usemail!(Map.get(params, "email"), Map.get(params, "username"))
    render(conn, "user.json", user: user)
  end

  def manager(conn, %{"userID" => id}) do
    user = Gotham.get_user!(id)

    with {:ok, %User{} = user} <- Gotham.set_manager_role(user) do
      render(conn, "user.json", user: user)
    end
  end

  def admin(conn, %{"userID" => id}) do
    user = Gotham.get_user!(id)

    with {:ok, %User{} = user} <- Gotham.set_admin_role(user) do
      render(conn, "user.json", user: user)
    end
  end

end
