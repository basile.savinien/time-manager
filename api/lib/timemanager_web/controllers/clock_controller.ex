  defmodule TimemanagerWeb.ClockController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger
  alias Timemanager.Gotham
  alias Timemanager.Gotham.Clock
  alias Timemanager.Gotham.User
  alias Timemanager.Repo

  action_fallback(TimemanagerWeb.FallbackController)

  def swagger_definitions do
    %{
      Clock:
        swagger_schema do
          title("Clock")
          description("Clock schema")

          properties do
            time(:datetime, "Clock's time", required: true)
            status(:boolean, "The status", required: true)
            user(:User, "The user", required: true)
          end

          example(%{
            clock: %{
              time: "2019-03-01 03:00:02.123+02:00",
              status: "true",
              user: "1"
            }
          })
        end
    }
  end

  swagger_path :create do
    tag("Clocks")
    post("/clocks/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
      clock(:body, Schema.ref(:Clock), "Create Clock", required: true)
    end

    response(200, "Success")
  end

  swagger_path :index do
    tag("Clocks")
    get("/clocks/{userId}")

    parameters do
      userId(:path, :string, "The user id", required: true)
    end

    response(200, "Success")
  end

  def index(conn, params) do
    user_id = Map.get(params, "userID")
    clocks = Gotham.get_clock_by_user_id_mult(user_id)
    render(conn, "index.json", clocks: clocks)
  end

  def create(conn, %{"userID" => id}) do
    user = Repo.get(User, id)
    check = Gotham.get_clock_by_user_id(id)
    IO.inspect(check)
    clock_params = %{time: NaiveDateTime.local_now(), status: true}

    if !is_nil(check) do
      IO.inspect(check)

      if check.status do
        Gotham.create_workingtime(%{start: check.time, end: NaiveDateTime.local_now()}, user)
        clock_params = %{time: NaiveDateTime.local_now(), status: false}

        with {:ok, %Clock{} = clock} <- Gotham.update_clock(check, clock_params) do
          conn
          |> put_status(:created)
          |> render("show.json", clock: clock)
        end
      else
        clock_params = %{time: NaiveDateTime.local_now(), status: true}

        with {:ok, %Clock{} = clock} <- Gotham.update_clock(check, clock_params) do
          conn
          |> put_status(:created)
          |> render("show.json", clock: clock)
        end
      end
    else
      with {:ok, %Clock{} = clock} <- Gotham.create_clock(clock_params, user) do
        conn
        |> put_status(:created)
        # |> put_resp_header("location", Routes.clock_path(conn, :show, id))
        |> render("show.json", clock: clock)
      end
    end
  end

  def show(conn, %{"id" => id}) do
    clock = Gotham.get_clock!(id)
    render(conn, "show.json", clock: clock)
  end

  def update(conn, %{"id" => id, "clock" => clock_params}) do
    clock = Gotham.get_clock!(id)

    with {:ok, %Clock{} = clock} <- Gotham.update_clock(clock, clock_params) do
      render(conn, "show.json", clock: clock)
    end
  end

  def delete(conn, %{"id" => id}) do
    clock = Gotham.get_clock!(id)

    with {:ok, %Clock{}} <- Gotham.delete_clock(clock) do
      render(conn, "show.json", clock: clock)
    end
  end
end
