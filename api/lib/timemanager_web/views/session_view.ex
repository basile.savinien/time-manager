defmodule TimemanagerWeb.SessionView do
  use TimemanagerWeb, :view

  def render("sign_in.json", %{user: user, jwt: jwt}) do
    %{
      data: %{
        token: jwt,
        },
    }
  end

  def render("error.json", %{message: message}) do
    %{
      message: message
    }
  end

  def render("sign_out.json", %{message: message}) do
    %{
      message: message
    }
  end
end