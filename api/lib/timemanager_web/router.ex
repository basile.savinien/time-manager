defmodule TimemanagerWeb.Router do
  use TimemanagerWeb, :router

  pipeline :api do
    plug(CORSPlug, origin: "*")
    plug(:accepts, ["json"])
    plug(Guardian.Plug.VerifyHeader, realm: "Bearer")
    plug(Guardian.Plug.LoadResource)
  end

  pipeline :authenticated do
    plug(Guardian.Plug.EnsureAuthenticated)
  end

  pipeline :admin do
    plug TimemanagerWeb.EnsureRolePlug, :admin
  end

  scope "/admin" do
    pipe_through [:authenticated, :admin]
  end

  scope "/api", TimemanagerWeb do
    pipe_through(:api)

    scope "/users" do
      post("/", UserController, :create)
      # options("/:userID", UserController, :options)
    end
    post("/sign_in", SessionController, :sign_in) # Add this line
    options("/sign_in", SessionController, :options)


    pipe_through :authenticated
    put("/admin/:userID", UserController, :admin)
    put("/manager/:userID", UserController, :manager)
    options("/manager/:userID", UserController, :options)
    get("/sign_out", SessionController, :sign_out)
    options("/sign_out", SessionController, :options)

    scope "/workingtimes" do
      get("/:userID", WorkingtimeController, :all)
      delete("/:id", WorkingtimeController, :delete)
      put("/:id", WorkingtimeController, :update)
      get("/:userID/:workingtimeID", WorkingtimeController, :index)
      post("/:userID", WorkingtimeController, :create)
      options("/:id", WorkingtimeController, :options)
    end

    scope "/clocks" do
      get("/:userID", ClockController, :index)
      post("/:userID", ClockController, :create)
      options("/:userID", ClockController, :options)
    end

    scope "/users" do
      get("/", UserController, :index)
      options("/", UserController, :options)
      get("/:userID", UserController, :show)
      put("/:userID", UserController, :update)
      delete("/:userID", UserController, :delete)
      options("/:userID", UserController, :options)
    end
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Timemanager API"
      },
      consumes: ["application/json"],
      produces: ["application/json"],
      basePath: "/api",
      tags: [
        %{name: "Users", description: "User resources"},
        %{name: "Clocks", description: "Clock resources"},
        %{name: "Workingtimes", description: "Workingtime resources"},
        %{name: "Sessions", description: "Sign in and Sign out"}
      ]
    }
  end

  scope "/api" do
    forward("/", PhoenixSwagger.Plug.SwaggerUI,
      otp_app: :timemanager,
      swagger_file: "swagger.json"
    )
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through([:fetch_session, :protect_from_forgery])
      live_dashboard("/dashboard", metrics: TimemanagerWeb.Telemetry)
    end
  end
end
