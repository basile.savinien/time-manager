#!/usr/bin/env bash
ssh root@64.227.97.126 /bin/bash <<'EOT'
pwd
ls -ail
cd ./time-manager
git stash
git pull
./install.sh
./start.sh
EOT
